#!/bin/bash

GITLAB_URL="https://gitlab.com"
REGISTRATION_TOKEN="glrt-tOkEn"

sudo gitlab-runner register --non-interactive \
  --url "$GITLAB_URL" \
  --registration-token "$REGISTRATION_TOKEN" \
  --executor "docker" \
  --docker-image alpine:latest \
  --description "docker-runner" \
  --tag-list "docker,aws" \
  --run-untagged="true" \
  --locked="false"
